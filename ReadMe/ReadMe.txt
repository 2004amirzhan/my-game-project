    Игра под названием [данные удалены] - от разработчиков такого говн... то есть, таких игр как Buglefield 2042, Fart Cry 6, GTA: The Trilogy – The Bugs Edition и других недоделанных кусков багов под издательством Microsoft это пошаговая игра во вселенной Трансформеры(Hasbro пж не засудите меня).

        Интерфейс игры:

            По углам находятся блоки с именем, типом(ниже есть более подробное описание) и здоровьем вашего персонажа/персонажа противника, при наведении можно получить больше информации об роботе.

            На экране есть два кибертронца, ваш бот и бот противника(может быть стикер вашего бота с инвертированным эффектом если автор не успел/поленился создать модель противника)

            Для получения большей информации наведите курсор на блок со здоровьем персонажа или на кнопку атаки

            У вас есть 4 кнопки:
                ● Атака - открывает меню атак персонажа.(ниже есть более подробное описание)
                ● Автоботы - открывает меню выбора других автоботов(временно не доступна).
                ● Инвентарь - открывает меню выбора предметов для лечения, усиления персонажей(временно не доступна).
                ● Выход - выход в главное меню.

            Атаки - обычно есть 5 кнопок:
                ● Назад - кнопка для возвращению к прошлому меню выбора.
                ● 3 вида атак - бывают разных типов влияющие на выдаваемые эффекты.
                ● Специальная атака - есть только у определенного бота, золотого цвета.

            Текстовое окно - выводит сообщения с использованной атакой, выбранным персонажем, использованным предметом, сообщает о промахе атакой/нанесению урона.

        Правила и механики:

            После каждого действия(атака, выбора персонажа, использования предмета) вы передаете ход противнику. После действия противника ход возвращается к игроку.

            Типы роботов:
                ● Легкий - маленький и с небольшим запасом здоровья, но быстрые и ловкие из-за чего тяжелым и не поворотливым ботом сложно по ним попасть.
                ● Средний - золотая середина, более быстрые и ловкие чтобы поймать "легких" роботов, но не достаточно и не достаточно толстой броней чтобы противостоять "тяжелым" братьям.
                ● Тяжелый - толстая броня и сильные атаки с использованием тяжелого и мощного оружия с большим уроном, но для этого нужно попасть по цели.
                ● Летающий - почти тоже что и "средний" тип, но имеет возможность перемещаться по воздуху из-за чего атаки ближнего боя очень не эффективны и имеют большой шанс промаха, но того же не скажешь насчет дальнобойных атак.
                ● [данные удалены] - https://youtu.be/PkT0PJwy8mI.

            Типы атак(временно не доступны), на данный момент есть 5 типов:
                ● Обычный - обычные атаки не имеющие эффектов, кнопка белого цвета.
                ● Энергонный - оружие состоящее из энергона лучше пробивает корпус кибертронцев и имеет больший урон нежели обычный тип, так же не имеет эффектов, кнопка оранжевого цвета.
                ● Огненный - имеет урон не намного больше обычного типа, но вызывает эффект горения на пару ходов наносящий урон цели, кнопка красного цвета.
                ● Шоковый - имеет урон не намного больше обычного типа, но вызывает эффект шокового удара на пару ходов из-за чего у цели увеличивается шанс промаха и блокируется специальная атака, кнопка желтого цвета.
                ● Специальный(Возможно в будущем не будет считаться "типом" атак) - имеет большой урон и может быть любого типа, при использовании проигрывает анимацию, кнопка золотого цвета не зависимо от других типов.

            Количество использовании(временно не доступны) - у всех атак кроме обычных есть количество использования для баланса, пример:
                ● Обычный - бесконечное количество.
                ● Энергонный - 15 использовании.
                ● Огненный - 10 использовании.
                ● Шоковый - 10 использовании.
                ● Специальный - 3 использовании.

    В этой же папке есть схема типов роботов и виды атак

    На этом всё, желаю приятной игры, и спасибо что выбрали эту игру)