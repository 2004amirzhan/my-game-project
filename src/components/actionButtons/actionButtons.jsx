import React from "react";
import "./ActionButtons.css";

const Action = ({ datas, actionBoxStyle, backFunction }) => {
  return (
    <div className="actionBox" style={actionBoxStyle}>
      {backFunction && (
        <button className="actionButton" onClick={backFunction}>
          Назад
        </button>
      )}
      {datas[0].text &&
        datas.map((data_array, i) => (
          <button
            key={i}
            id="Button"
            title={data_array?.title}
            className="actionButton"
            onClick={data_array?.onAction}
          >
            {data_array.text}
          </button>
        ))}
      {datas[0].data &&
        datas.map((data_array, i) => (
          <button
            key={i}
            id="Button"
            title={`Название: ${data_array?.data?.name}\nШанс промаха: ${
              100 - data_array?.data?.miss
            }%\nУрон: ${data_array?.data?.damage}`}
            className="actionButton"
            onClick={data_array?.onAction}
          >
            {data_array?.data?.name}
          </button>
        ))}
    </div>
  );
};

export default Action;
