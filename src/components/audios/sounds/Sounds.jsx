import React from "react";

const Sounds = () => {
  return (
    <>
      <audio
        id="damage"
        src="./audios/damage.ogg"
        data-sound
        preload="auto"
        loop
      />
      <audio id="miss" src="./audios/miss.ogg" data-sound preload="auto" loop />

      <audio
        id="TrypticonLaughter"
        src="./audios/trypticon-laughter.ogg"
        data-sound
        preload="auto"
        loop
      />
    </>
  );
};

export default Sounds;
