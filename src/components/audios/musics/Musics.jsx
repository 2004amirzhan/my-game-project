import React from "react";

const Musics = ({ sceneNow }) => {
  return (
    <>
      {sceneNow === "Level_1_1" && (
        <audio
          id="fightTheme0"
          src="./audios/musics/Godrays-ENTITY.mp3"
          data-music
          preload="auto"
          loop
        />
      )}
      {sceneNow === "Level_1_0" && (
        <audio
          id="tuTorielTheme"
          src="./audios/musics/tuToriel-theme.mp3"
          data-music
          preload="auto"
          loop
        />
      )}
      {sceneNow === "Menu" && (
        <audio
          id="MenuTheme"
          src="./audios/musics/menu-theme.mp3"
          data-music
          preload="auto"
          loop
        />
      )}
    </>
  );
};

export default Musics;
