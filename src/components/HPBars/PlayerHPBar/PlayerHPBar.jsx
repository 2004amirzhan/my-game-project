import React from "react";
import "./PlayerHPBar.css";
import "../HPBars.css";
import onTypeTest from "../../../functions/onTypeTest";

const PlayerHPBar = ({ autobot, playerHP, playerLineHP }) => {
  return (
    <div
      className="playerHP boardHP"
      title={`Имя: ${autobot.name}\nФракция: ${autobot.fractionRU}\nДолжность: ${autobot.post}\nТип: ${autobot.type}`}
    >
      <h1>
        {autobot.name} {onTypeTest(autobot.type, "autobot")}
      </h1>
      <h1 style={{ marginTop: "0px", fontSize: "20px" }}>
        {playerHP}/{autobot.HP} HP
      </h1>
      <div
        style={{
          margin: "5px 15px 15px",
          background: "rgb(128, 128, 128)",
          border: "rgb(63, 63, 63) 3px solid",
        }}
      >
        <div
          className="lineHP"
          style={{
            marginRight: playerLineHP <= 100 ? playerLineHP + "%" : "100%",
            background: "rgb(0, 223, 0)",
            outline:
              playerLineHP < 100
                ? "rgb(0, 133, 0) 3px solid"
                : "rgb(0, 133, 0) 0px solid",
          }}
        />
      </div>
    </div>
  );
};

export default PlayerHPBar;
