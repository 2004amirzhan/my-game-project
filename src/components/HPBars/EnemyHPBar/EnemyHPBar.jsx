import React from "react";
import "./EnemyHPBar.css";
import "../HPBars.css";
import onTypeTest from "../../../functions/onTypeTest";

const EnemyHPBar = ({ decepticon, enemyLineHP }) => {
  return (
    <div
      className="enemyHP boardHP"
      title={`Имя: ${decepticon.name}\nФракция: ${decepticon.fractionRU}\nДолжность: ${decepticon.post}\nТип: ${decepticon.type}`}
    >
      <h1>
        {decepticon.name} {onTypeTest(decepticon.type, "decepticon")}{" "}
      </h1>
      <div
        style={{
          margin: "5px 15px 15px",
          background: "rgb(128, 128, 128)",
          border: "rgb(63, 63, 63) 3px solid",
        }}
      >
        <div
          className="lineHP"
          style={{
            marginRight: enemyLineHP <= 100 ? enemyLineHP + "%" : "100%",
            background: "rgb(223, 0, 0)",
            outline:
              enemyLineHP < 100
                ? "rgb(133, 0, 0) solid 3px"
                : "rgb(133, 0, 0) solid 0px",
          }}
        />
      </div>
    </div>
  );
};

export default EnemyHPBar;
