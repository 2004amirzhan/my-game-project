import React from "react";

const SpritesArena = ({ id, fraction, spriteName, actionSprite }) => {
  return (
    <>
      <img
        style={{ display: actionSprite === 0 ? "" : "none" }}
        id={id}
        className={id}
        src={`/images/characters/${fraction}s/${spriteName}/sprites/${spriteName}-arena.png`}
        alt="img"
      />
      <img
        style={{ display: actionSprite === -1 ? "" : "none" }}
        id={id + "-damage"}
        className={id}
        src={`/images/characters/${fraction}s/${spriteName}/sprites/${spriteName}-damage.png`}
        alt="img"
      />
      <img
        style={{ display: actionSprite === 1 ? "" : "none" }}
        id={id + "-attack1"}
        className={id}
        src={`/images/characters/${fraction}s/${spriteName}/sprites/${spriteName}-attack1.png`}
        alt="img"
      />
      <img
        style={{ display: actionSprite === 2 ? "" : "none" }}
        id={id + "-attack2"}
        className={id}
        src={`/images/characters/${fraction}s/${spriteName}/sprites/${spriteName}-attack2.png`}
        alt="img"
      />
      <img
        style={{ display: actionSprite === 3 ? "" : "none" }}
        id={id + "-attack3"}
        className={id}
        src={`/images/characters/${fraction}s/${spriteName}/sprites/${spriteName}-attack3.png`}
        alt="img"
      />

      <img
        style={{ display: actionSprite === 4 ? "" : "none" }}
        id={id + "-attack4"}
        className={id}
        src={`/images/characters/${fraction}s/${spriteName}/sprites/${spriteName}-attack4.png`}
        alt="img"
      />
    </>
  );
};

export default SpritesArena;
