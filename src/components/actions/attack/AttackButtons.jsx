import React from "react";

const data = ({ datas, backFunction, actionBoxStyle }) => {
  return (
    <div className="actionBox" style={actionBoxStyle}>
      <button className="actionButton" onClick={backFunction}>
        Назад
      </button>
      {datas.map((data_array, i) => (
        <button
          key={i}
          id="Button"
          title={`Название: ${data_array?.data.name}\nШанс промаха: ${
            100 - data_array?.data?.miss
          }%\nУрон: ${data_array?.data?.damage}`}
          className="actionButton"
          onClick={data_array?.onAttacks}
        >
          {data_array.data.name}
        </button>
      ))}
    </div>
  );
};

export default data;
