import React, { useEffect, useState } from "react";
import "./LevelResult.css";

const LevelResult = ({ playerEndLevel, sceneTransition }) => {
  const [exitAsk, setExitAsk] = useState(false);

  useEffect(() => {
    const resultText = document.getElementById("resultText");
    if (playerEndLevel.result === "win" && exitAsk === false) {
      resultText.textContent = "Вы победили";
    } else if (playerEndLevel.result === "lose" && exitAsk === false) {
      resultText.textContent = "Вы проиграли";
    } else if (exitAsk === true) {
      resultText.textContent = "Даныне будут утеряны, вы уверены? ";
    }
  });

  const nextLevel = () => {
    sceneTransition("Level_1_1");
  };

  const restartLevel = () => {
    sceneTransition(
      "Level_" + playerEndLevel.level + "_" + playerEndLevel.chapter
    );
  };

  return (
    <div className="resultDiv">
      <span className="blackout" />
      <div
        className="resultMenuCase"
        style={{
          background: "url(images/textures/menu/teletran-1.png) 50% 0",
          backgroundSize: "400%",
        }}
      >
        <div className="resultMenu">
          <h1 id="resultText">Вы победили</h1>
          <div className="resultBlock">
            <div className="resultButtons">
              {exitAsk === false && (
                <button
                  onClick={() => {
                    setExitAsk(true);
                  }}
                >
                  Выйти
                </button>
              )}
              {playerEndLevel.result === "win" && exitAsk === false && (
                <button onClick={() => nextLevel()}>Продолжить</button>
              )}
              {playerEndLevel.result === "lose" && exitAsk === false && (
                <button onClick={() => restartLevel()}>Повторить</button>
              )}

              {exitAsk === true && (
                <button onClick={() => sceneTransition("Menu")}>Да</button>
              )}
              {exitAsk === true && (
                <button onClick={() => setExitAsk(false)}>Нет</button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LevelResult;
