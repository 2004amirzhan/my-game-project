import React, { useEffect, useState } from "react";
import "./SettingsWindow.css";

const SettingsWindow = ({ showGlobalElement, setVolume, volume }) => {
  const [volumeLevel, setVolumeLevel] = useState(0);

  useEffect(() => {
    const screenMode = document.getElementById("screenMode");
    if (!document.fullscreenElement) {
      screenMode.textContent = "Оконный";
    } else {
      screenMode.textContent = "Полный экран";
    }
  });

  useEffect(() => {
    document.getElementById("volume").value = volume;
    setVolumeLevel(volume);
  }, [volume]);

  const onApply = () => {
    setVolume(document.getElementById("volume").value);
    if (
      document.getElementById("screenMode").textContent === "Полный экран" &&
      !document.fullscreenElement
    ) {
      document.documentElement.requestFullscreen();
    } else if (
      document.getElementById("screenMode").textContent === "Оконный" &&
      document.fullscreenElement
    ) {
      document.exitFullscreen();
    }
  };

  const onReset = () => {
    const screenMode = document.getElementById("screenMode");
    const volumeValue = document.getElementById("volume");
    volumeValue.value = 50;
    setVolume(volumeValue.value);
    setVolumeLevel(volume);
    if (document.fullscreenElement) {
      document.exitFullscreen();
      screenMode.textContent = "Оконный";
    }
  };

  const selectedScreenMode = () => {
    const screenMode = document.getElementById("screenMode");
    if (screenMode.textContent === "Полный экран") {
      screenMode.textContent = "Оконный";
    } else {
      screenMode.textContent = "Полный экран";
    }
  };

  return (
    <div className="settingsDiv">
      <span className="blackout" />
      <div
        className="settingsMenuCase"
        style={{
          background: "url(images/textures/menu/teletran-1.png) 50% 0",
          backgroundSize: "400%",
        }}
      >
        <div className="settingsMenu">
          <h1>Настройки</h1>
          <div className="settingsBlock">
            <p>Звук</p>
            <input
              type="range"
              min="0"
              max="100"
              id="volume"
              onChange={(event) => setVolumeLevel(event.target.value)}
            />
            <p>{volumeLevel}%</p>
          </div>
          <div className="settingsBlock">
            <p>Режим экрана</p>
            <div className="selectedBox">
              <button id="leftButton" onClick={() => selectedScreenMode()}>
                &#8249;
              </button>
              <p id="screenMode">Оконный</p>
              <button id="rightButton" onClick={() => selectedScreenMode()}>
                &#8250;
              </button>
            </div>
          </div>
          <div className="settingsButtons">
            <button onClick={() => onReset()}>Сбросить</button>
            <button onClick={() => onApply()}>Применить</button>
            <button onClick={showGlobalElement}>Закрыть</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SettingsWindow;
