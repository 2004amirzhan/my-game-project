import React from "react";
import "./AnswersWindow.css";

const Answers = ({ onYes, onNo }) => {
  return (
    <div className="answersDiv">
      <span className="blackout" />
      <div className="answersMenu">
        <h1>Вы уверены?</h1>
        <div className="answersBlock">
          <p>Не сохраненные данные будут потеряны!</p>
          <div>
            <button className="answers" onClick={onYes}>
              Да
            </button>
            <button className="answers" onClick={onNo}>
              Нет
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Answers;
