import React from "react";
import "./Preloader.css";

const Preloader = ({setPreloader}) => {
  setTimeout(() => {
    const preloader = document.getElementById("Preloader");
    if (!preloader.classList.contains("hide")) {
      preloader.classList.add("hide");
      setTimeout(() => {
        setPreloader(false)
      }, 500);
    }
  }, 6000);

  return (
    <>
      <div id="Preloader">
        <div className="loader" />
      </div>
    </>
  );
};

export default Preloader;
