import React, { useState, useEffect } from "react";

// ---------------------- imports preloader ---------------------- //
import Preloader from "./components/preloader/Preloader.jsx";

// ---------------------- imports scenes ---------------------- //
import MenuScene from "./scene/MenuScene/MenuScene.jsx";
import LevelFirst0 from "./levels/levels_1/Level_1_0.jsx";
import LevelFirst1 from "./levels/levels_1/Level_1_1.jsx";
import IntroScene from "./scene/IntroScene/IntroScene.jsx";

// ---------------------- imports components ---------------------- //
import SettingsWindow from "./components/actions/SettingsWindow/SettingsWindow.jsx";
import Sounds from "./components/audios/sounds/Sounds.jsx";
import Musics from "./components/audios/musics/Musics.jsx";
import LevelResult from "./components/actions/levelResult/LevelResult.jsx";
import AnswersWindow from "./components/actions/AnswersWindow/AnswersWindow.jsx";

const PlayGame = () => {
  const [volume, setVolume] = useState(50);
  const [preloader, setPreloader] = useState(true);
  const [sceneNow, setSceneNow] = useState("Menu");
  const [showGlobalElement, setShowGlobalElement] = useState("");
  const [playerEndLevel, setPlayerEndLevel] = useState({});

  useEffect(() => {
    document.querySelectorAll("audio").forEach((audio) => {
      audio.volume = volume / 100;
    });
  }, [volume]);

  const audioPlay = (audioName) => {
    setTimeout(() => {
      document.getElementById(audioName).play();
    }, 1000);
  };

  const sceneTransition = (sceneName) => {
    setPreloader(true);
    setSceneNow(sceneName);
    setShowGlobalElement("");
  };

  return (
    <>
      {preloader === true && <Preloader setPreloader={setPreloader} />}
      <Sounds />
      <Musics sceneNow={sceneNow} />
      {sceneNow === "Intro" && <IntroScene sceneTransition={sceneTransition} />}
      {showGlobalElement === "settingsWindow" && preloader === false && (
        <SettingsWindow
          showGlobalElement={() => setShowGlobalElement("")}
          setVolume={setVolume}
          volume={volume}
        />
      )}
      {showGlobalElement === "answersWindow" && preloader === false && (
        <AnswersWindow setSceneNow={setSceneNow} />
      )}
      {preloader === false && (
        <button
          className="settings"
          onClick={() => setShowGlobalElement("settingsWindow")}
        >
          &#128736;
        </button>
      )}
      {sceneNow === "Menu" && (
        <MenuScene sceneTransition={sceneTransition} audioPlay={audioPlay} />
      )}
      {sceneNow === "Level_1_0" && (
        <LevelFirst0
          audioPlay={audioPlay}
          setPlayerEndLevel={setPlayerEndLevel}
          setShowGlobalElement={setShowGlobalElement}
        />
      )}
      {sceneNow === "Level_1_1" && <LevelFirst1 audioPlay={audioPlay} />}
      {showGlobalElement === "levelResult" && (
        <LevelResult
          playerEndLevel={playerEndLevel}
          sceneTransition={sceneTransition}
        />
      )}
    </>
  );
};

export default PlayGame;
