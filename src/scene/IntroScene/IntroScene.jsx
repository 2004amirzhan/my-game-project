import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./IntroScene.css";

// ---------------------- imports preloader ---------------------- //
import Preloader from "../../components/preloader/Preloader";

const MenuScene = () => {
  useEffect(() => {
    document.body.style.background = "#000000";
    setTimeout(() => {
      playVideo();
    }, 6000);
  }, []);

  let navigate = useNavigate();

  const onPageChange = (page) => {
    setTimeout(() => {
      return navigate(page);
    }, 1000);
  };

  

  setTimeout(() => {
    document.getElementById("myVideo").addEventListener("ended", () => {
      document.getElementById("myVideo").pause();
      setTimeout(() => {
        onPageChange("/menu");
      }, 1000);
    });
  }, 1000);

  const playVideo = () => {
    setTimeout(() => {
      document.getElementById("myVideo").play();
    }, 1000);
  };

  return (
    <div
      className="mainDiv"
      style={{ position: "relative", textAlign: "center" }}
    >
      <Preloader />
      <video
        style={{
          margin: "0 auto",
          height: "600px",
        }}
        id="myVideo"
        src="/videos/videoTest.mp4 "
      />
    </div>
  );
};

export default MenuScene;
