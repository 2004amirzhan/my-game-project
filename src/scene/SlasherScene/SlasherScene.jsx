// import React, { useEffect, useState } from "react";
// import EnemyHPBar from "../../components/HPBars/EnemyHPBar/EnemyHPBar";
// import PlayerHPBar from "../../components/HPBars/PlayerHPBar/PlayerHPBar";
// // import getRandom from "../../functions/getRandom";
// import "./SlasherScene.css";
// // ---------------------- imports characters ---------------------- //
// import decepticons from "../../datas/characters/decepticons/decepticons";
// import autobots from "../../datas/characters/autobots/autobots";

// const SlasherScene = () => {
//   const [playerHP, setPlayerHP] = useState(autobots.Grimlock.HP);
//   const [enemyHP, setEnemyHP] = useState(decepticons.Bruticus.HP);
//   const [playerLineHP, setPlayerLineHP] = useState(0);
//   const [enemyLineHP, setEnemyLineHP] = useState(0);

//   useEffect(() => {
//     document.body.style.background = "gray";
//   }, []);

//   document.body.oncontextmenu = () => {
//     return false;
//   };

//   let leftClick = false;
//   let rightPress = false;
//   let rightNotPress = false;

//   document.body.addEventListener("mousedown", (e) => {
//     const GrimlockSrc = document.getElementById("Grimlock");
//     // const BruticusSrc = document.getElementById("Bruticus");
//     if (e.which === 1 && leftClick === false && rightPress === false) {
//       leftClick = true;
//       GrimlockSrc.style.backgroundPositionX = "-35vw";
//       setTimeout(() => {
//         GrimlockSrc.style.backgroundPositionX = "-105vw";
//         setTimeout(() => {
//           GrimlockSrc.style.backgroundPositionX = "0";
//           leftClick = false;
//         }, 350);
//       }, 500);
//     }
//     if (e.which === 3 && leftClick === false && rightPress === false) {
//       rightPress = true;
//       rightNotPress = false;
//       GrimlockSrc.style.backgroundPositionX = "-70vw";
//       BruticusAttack();
//     }
//   });

//   document.body.addEventListener("mouseup", (e) => {
//     const GrimlockSrc = document.getElementById("Grimlock");
//     // const BruticusSrc = document.getElementById("Bruticus");
//     if (e.which === 3 && rightPress === true && rightNotPress === false) {
//       rightNotPress = true;
//       rightPress = false;
//       GrimlockSrc.style.backgroundPositionX = "0";
//     }
//   });

//   const BruticusAttack = () => {
//     const BruticusSrc = document.getElementById("Bruticus");
//     // const attackSelect = getRandom();
//     setTimeout(() => {
//       BruticusSrc.style.backgroundPositionX = "-100vw";
//       setTimeout(() => {
//         BruticusSrc.style.backgroundPositionX = "-150vw";
//         setTimeout(() => {
//           BruticusSrc.style.backgroundPositionX = "0";
//         }, 500);
//       }, 500);
//     }, 500);
//   };

//   return (
//     <div className="mainDiv">
//       <span className="divClicker" />
//       <EnemyHPBar decepticon={decepticons.Bruticus} enemyLineHP={enemyLineHP} />
//       <PlayerHPBar
//         autobot={autobots.Grimlock}
//         playerHP={playerHP}
//         playerLineHP={playerLineHP}
//       />
//       <div className="arena">
//         <div
//           id="Bruticus"
//           className="Bruticus divImage"
//           style={{
//             background: "url(images/slasher/bruticus.png) 0px 0px / cover",
//           }}
//         />
//         <div
//           id="Grimlock"
//           className="Grimlock divImage"
//           style={{
//             background: "url(images/slasher/grimlock.png) 0px 0px / cover",
//           }}
//         />
//       </div>
//     </div>
//   );
// };

// export default SlasherScene;
