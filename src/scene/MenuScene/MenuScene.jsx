import React, { useEffect } from "react";
// import { useNavigate } from "react-router-dom";
import "./MenuScene.css";

// ---------------------- imports preloader logic ---------------------- //
// import Preloader from "../../components/preloader/Preloader";

const MenuScene = ({ sceneTransition, audioPlay }) => {
  useEffect(() => {
    document.body.style.background =
      "url(images/textures/menu/teletran-1.png) 0 10% / 100% ";
    audioPlay("MenuTheme");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // let navigate = useNavigate();

  // const onPageChange = (page) => {
  //   return navigate(page);
  // };

  return (
    <div className="mainDiv">
      <div className="teletranScreen">
        <div className="buttonsContainer">
          <button
            onClick={() => {
              sceneTransition("Level_1_0");
            }}
          >
            Начать
          </button>
          {/* <button
            onClick={() => {
              setSceneNow("");
            }}
          >
            Заставка
          </button> */}
        </div>
      </div>
    </div>
  );
};

export default MenuScene;
