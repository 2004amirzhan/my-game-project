import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./TutorialScene.css";
import "../FightScene/FightScene.css";

// ---------------------- imports buttons ---------------------- //
import ActionButtons from "../../components/actionButtons/ActionButtons.jsx";
import AnswersWindow from "../../components/actions/AnswersWindow/AnswersWindow.jsx";

// ---------------------- imports game logic ---------------------- //
import onPercentHP from "../../functions/onPercentHP";
import getRandom from "../../functions/getRandom";
import playSoundClick from "../../functions/playSoundClick";
import EnemyHPBar from "../../components/HPBars/EnemyHPBar/EnemyHPBar";
import PlayerHPBar from "../../components/HPBars/PlayerHPBar/PlayerHPBar";

const TutorialScene = ({
  autobots,
  decepticons,
  audioPlay,
  setPlayerEndLevel,
  setShowGlobalElement,
}) => {
  const [showLocalElement, setShowLocalElement] = useState("");
  const [enemyMove, setEnemyMove] = useState(false);
  const [playerHP, setPlayerHP] = useState(autobots.HP);
  const [enemyHP, setEnemyHP] = useState(decepticons.HP);
  const [playerLineHP, setPlayerLineHP] = useState(0);
  const [enemyLineHP, setEnemyLineHP] = useState(0);
  const [message, setMessage] = useState("");

  const playerSrc = document.getElementById("Player");
  const enemySrc = document.getElementById("Enemy");

  let navigate = useNavigate();

  const onPageChange = (page) => {
    setTimeout(() => {
      return navigate(page);
    }, 1000);
  };

  useEffect(() => {
    const playerSrc = document.getElementById("Player");
    // const enemySrc = document.getElementById("Enemy");
    audioPlay("tuTorielTheme");
    document.body.style.background = "rgb(0, 139, 219)";
    setPlayerHP(autobots.HP);
    setEnemyHP(decepticons.HP);
    setEnemyLineHP(0);
    setPlayerLineHP(0);
    setMessage("");
    setShowLocalElement("");
    // enemySrc.style.backgroundPositionX = "0px";
    // document.getElementById("Arena").style.opacity = "0";

    setTimeout(() => {
      playerSrc.classList.add("transform");
      setTimeout(() => {
        playerSrc.classList.remove("transform");
        playerSrc.classList.add("calm");
      }, 1456);
    }, 1000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onPlayerAttack = (playerAttackData) => {
    playerSrc.classList.add(`pull${playerAttackData.attackNumber}Weapon`);
    setEnemyMove(true);
    setShowLocalElement("");
    setMessage(`${autobots.name} использовал атаку "${playerAttackData.name}"`);
    document.getElementsByTagName("html")[0].style.cursor = "progress";
    setTimeout(() => {
      playerSrc.classList.remove(`pull${playerAttackData.attackNumber}Weapon`);
      playerSrc.classList.add(`attack${playerAttackData.attackNumber}Weapon`);
      if (getRandom() <= playerAttackData.miss) {
        const enemyHPTemp = enemyHP - playerAttackData.damage;
        setEnemyHP(enemyHPTemp);
        setEnemyLineHP(
          onPercentHP(decepticons.HP, playerAttackData.damage, enemyLineHP)
        );
        enemySrc.style.backgroundPositionX = "-900px";
        setTimeout(() => {
          enemySrc.classList.add("damage");
          playSoundClick("audios/damage.ogg");
          setTimeout(() => {
            playerSrc.classList.remove(
              `attack${playerAttackData.attackNumber}Weapon`
            );
            playerSrc.classList.add(
              `hide${playerAttackData.attackNumber}Weapon`
            );
            enemySrc.classList.remove("damage");
            enemySrc.style.backgroundPositionX = "0px";
            setTimeout(() => {
              playerSrc.classList.remove(
                `hide${playerAttackData.attackNumber}Weapon`
              );
              testHP(enemyHPTemp);
            }, 3500);
          }, 1200);
        }, 280);
        return setMessage(
          `Атака "${playerAttackData.name}" нанесла урон ${decepticons.name}у`
        );
      } else {
        playerSrc.classList.add("attackPlayerAnim");
        setTimeout(() => {
          enemySrc.classList.add("miss");
          playSoundClick("audios/miss.ogg");
          setTimeout(() => {
            playerSrc.classList.remove(
              `attack${playerAttackData.attackNumber}Weapon`
            );
            playerSrc.classList.add(
              `hide${playerAttackData.attackNumber}Weapon`
            );
            playerSrc.classList.remove("attackPlayerAnim");
            enemySrc.classList.remove("miss");
            setTimeout(() => {
              playerSrc.classList.remove(
                `hide${playerAttackData.attackNumber}Weapon`
              );
              setEnemyMove(false);
              document.getElementsByTagName("html")[0].style.cursor = "";
            }, 3500);
          }, 1200);
        }, 350);
        return setMessage(
          `${decepticons.name} увернулся от атаки "${playerAttackData.name}"`
        );
      }
    }, 2000);
  };

  const testHP = (enemyHPTemp) => {
    document.getElementsByTagName("html")[0].style.cursor = "";
    if (enemyHPTemp > 0) {
      setEnemyMove(false);
    } else {
      if (enemyHPTemp <= 0) {
        setPlayerEndLevel({ level: "1", chapter: "0", result: "win" });
        setTimeout(() => {
          setShowGlobalElement("levelResult");
        }, 1000);
      }
      if (playerHP <= 0) {
        setPlayerEndLevel({ level: "1", chapter: "0", result: "lose" });
        setTimeout(() => {
          setShowGlobalElement("levelResult");
        }, 1000);
      }
    }
  };

  return (
    <div id="mainDiv" className="mainDiv">
      <div style={{ margin: "10px" }}></div>
      {showLocalElement === "answersWindow" && (
        <AnswersWindow
          onYes={() => onPageChange("/Menu")}
          onNo={() => setShowLocalElement("")}
        />
      )}
      <EnemyHPBar decepticon={decepticons} enemyLineHP={enemyLineHP} />
      <PlayerHPBar
        autobot={autobots}
        playerHP={playerHP}
        playerLineHP={playerLineHP}
      />
      <div id="Arena" className="arena">
        <div
          style={{
            background: `url(images/characters/decepticons/${decepticons.sprite}) 0px 0px`,
          }}
          className="Enemy divImage"
          id="Enemy"
        />
        <div
          style={{
            background: `url(images/characters/autobots/${autobots.sprite}) 0px 0px`,
          }}
          className="Player divImage"
          id="Player"
        />
      </div>
      <div className="board">
        <div
          className="messageBox"
          style={{
            outline: message !== "" ? "2px black solid" : "0px black solid",
          }}
        >
          <p style={{ margin: message !== "" ? "10px" : "0" }}>{message}</p>
        </div>
        {showLocalElement === "" && enemyMove !== true && (
          <ActionButtons
            actionBoxStyle={{ bottom: "2%" }}
            datas={[
              {
                text: "Атака",
                onAction: () => {
                  setShowLocalElement("attack");
                  setMessage("");
                },
              },
              {
                text: "Действие",
                onAction: () => {
                  setShowLocalElement("action");
                  setMessage("");
                },
              },
              {
                text: "Выход",
                onAction: () => {
                  setShowLocalElement("answersWindow");
                },
              },
            ]}
          />
        )}
        {showLocalElement === "attack" && enemyMove !== true && (
          <ActionButtons
            actionBoxStyle={{ bottom: "2%" }}
            backFunction={() => setShowLocalElement("")}
            datas={[
              {
                data: autobots.attacks.attack1,
                onAction: () => {
                  onPlayerAttack(autobots.attacks.attack1);
                },
              },
              {
                data: autobots.attacks.attack2,
                onAction: () => {
                  onPlayerAttack(autobots.attacks.attack2);
                },
              },
            ]}
          />
        )}
      </div>
    </div>
  );
};

export default TutorialScene;
