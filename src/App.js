import React from "react";
import { Routes, Route } from "react-router-dom";
import PlayGame from "./PlayGame.jsx";
import "./commonStyles/commonStyles.css";

window.onbeforeunload = () => {
  return false;
};

const App = () => {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<PlayGame />} />
      </Routes>
    </div>
  );
};

export default App;
