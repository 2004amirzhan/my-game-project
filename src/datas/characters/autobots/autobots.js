const autobots = {
  tutorialBot: {
    name: "Автобот",
    fractionEn: "autobot",
    fractionRU: "Автобот",
    post: "Новобранец",
    type: "Легкий",
    level: 1,
    sprite: "tutorialBot/tutorialBot-sprite-sheet.png",
    HP: 17,
    attacks: {
      attack1: {
        name: "Удар кулаком",
        miss: 80,
        damage: 2,
        attackNumber: "First",
      },
      attack2: {
        name: "Бластер",
        miss: 80,
        damage: 4,
        attackNumber: "Second",
      },
    },
    attacksAltmode: {
      attack1: {
        name: "Сбить",
        miss: 70,
        damage: 3,
        attackNumber: "First",
      },
    },
    actions: {
      action1: {
        name: "Трансформироваться",
      },
    },
  },
  Bumblebee: {
    name: "Бамблби",
    fractionEn: "autobot",
    fractionRU: "Автобот",
    post: "Разведчик",
    type: "Легкий",
    level: 1,
    sprite: "/bumblebee/bumblebee-sprite-sheet.png",
    HP: 17,
    attacks: {
      attack1: {
        name: "Клинок",
        miss: 90,
        damage: 2,
        attackNumber: "First",
      },
      attack2: {
        name: "Бластер",
        miss: 70,
        damage: 4,
        attackNumber: "Second",
      },
      attack3: {
        name: "Сбить альтформой",
        miss: 55,
        damage: 6,
        attackNumber: "Third",
      },
      attack4: {
        name: "Жало",
        miss: 100,
        damage: 90,
        attackNumber: "Fourth",
      },
    },
    attacksAltmode: {
      attack1: {
        name: "Сбить",
        miss: 70,
        damage: 3,
        attackNumber: "First",
      },
    },
    actions: {
      action1: {
        name: "Трансформироваться",
      },
    },
  },
  Ironhide: {
    name: "Айронхайд",
    fractionEn: "autobot",
    fractionRU: "Автобот",
    post: "Воин",
    type: "Средний",
    level: 1,
    sprite: "ironhide/ironhide-sprite-sheet.png",
    HP: 35,
    attacks: {
      attack1: {
        name: "Энергонный клинок",
        miss: 5,
        damage: 20,
        attackNumber: "First",
      },
      attack2: {
        name: "Плазменные пушки",
        miss: 20,
        damage: 30,
        attackNumber: "Second",
      },
      attack3: {
        name: "Ракетница",
        miss: 60,
        damage: 40,
        attackNumber: "Third",
      },
      attack4: { name: "Жало", miss: 85, damage: 150, attackNumber: "Fourth" },
    },
    attacksAltmode: {
      attack1: {
        name: "Сбить",
        miss: 60,
        damage: 4,
        attackNumber: "First",
      },
    },
  },
  Grimlock: {
    name: "Гримлок",
    fractionEn: "autobot",
    fractionRU: "Автобот",
    post: "Воин, лидер диноботов",
    type: "Тяжелый",
    level: 1,
    sprite: "grimlock",
    HP: 120,
  },
};

export default autobots;
