const decepticons = {
  decepticonModel: {
    name: "Десептикон манекен",
    fractionEn: "decepticon",
    fractionRU: "Десептикон",
    post: "Солдат",
    type: "Средний",
    sprite: "decepticonModel/decepticonModel-sprite-sheet.png",
    HP: 10,
  },
  VehiconWarriorGround: {
    name: "Вехикон",
    fractionEn: "decepticon",
    fractionRU: "Десептикон",
    post: "Солдат",
    type: "Средний",
    level: 1,
    sprite: "vehicon/vehicons-arena.png",
    HP: 15,
    attacks: {
      attack1: {
        name: "Энергонные клинки",
        miss: 90,
        damage: 2,
        attackNumber: "First",
      },
      attack2: { name: "Бластер", miss: 80, damage: 4, attackNumber: "Second" },
    },
  },
  VehiconWarriorFly: {
    name: "Вехикон",
    fractionEn: "decepticon",
    fractionRU: "Десептикон",
    post: "Солдат",
    type: "Летающий",
    level: 1,
    sprite: "",
    HP: 15,
    attacks: {
      attack1: {
        name: "Энерго клинки",
        miss: 90,
        damage: 10,
        attackNumber: "First",
      },
      attack2: {
        name: "Лазерный бластер",
        miss: 80,
        damage: 15,
        attackNumber: "Second",
      },
    },
  },
  VehiconWarriorElite: {
    name: "Вехикон",
    fractionEn: "decepticon",
    fractionRU: "Десептикон",
    post: "Элитный солдат",
    type: "Летающий",
    level: 1,
    sprite: "",
    HP: 28,
    attacks: {
      attack1: {
        name: "Энерго клинки",
        miss: 90,
        damage: 10,
        attackNumber: "First",
      },
      attack2: {
        name: "Лазерный бластер",
        miss: 80,
        damage: 15,
        attackNumber: "Second",
      },
    },
  },
  Starscream: {
    name: "Старскрим",
    fractionEn: "decepticon",
    fractionRU: "Десептикон",
    post: "Сикер",
    type: "Летающий",
    level: 1,
    sprite: "",
    HP: 30,
    attacks: {
      attack1: {
        name: "Нуль-лучевые пушки",
        miss: 45,
        damage: 20,
        attackNumber: "First",
      },
      attack2: { name: "Дрель", miss: 80, damage: 10, attackNumber: "Second" },
      attack3: {
        name: "Ракетницы",
        miss: 30,
        damage: 40,
        attackNumber: "Third",
      },
      attack4: {
        name: "пушка Мегатарон",
        miss: 15,
        damage: 150,
        attackNumber: "Fourth",
      },
    },
  },
  Soundwave: {
    name: "Саундвейв",
    fractionEn: "decepticon",
    fractionRU: "Десептикон",
    post: "Разведчик",
    type: "Средний",
    level: 3,
    sprite: "",
    HP: 45,
    attacks: {
      attack1: {
        name: "Импульсный бластер",
        miss: 45,
        damage: 20,
        attackNumber: "First",
      },
      attack2: {
        name: "Звуковая пушка",
        miss: 80,
        damage: 10,
        attackNumber: "Second",
      },
      attack3: {
        name: "Наплечная пушка",
        miss: 30,
        damage: 40,
        attackNumber: "Third",
      },
      attack4: {
        name: "Кассетники",
        miss: 15,
        damage: 150,
        attackNumber: "Fourth",
      },
    },
  },
  Bruticus: {
    name: "Брутикус",
    fractionEn: "decepticon",
    fractionRU: "Десептикон",
    post: "Комбайтикон",
    type: "Тяжелый",
    sprite: "",
    HP: 300,
  },
};

export default decepticons;
