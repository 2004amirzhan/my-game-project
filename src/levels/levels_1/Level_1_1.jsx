import React from "react";
import FightScene from "../../scene/FightScene/FightScene.jsx";

// ---------------------- imports characters ---------------------- //
import decepticons from "../../datas/characters/decepticons/decepticons";
import autobots from "../../datas/characters/autobots/autobots";

const Level_1_1 = () => {
  return (
    <>
      <FightScene
        autobots={autobots.Bumblebee}
        decepticons={decepticons.VehiconWarriorGround}
      />
    </>
  );
};

export default Level_1_1;
