import React from "react";
import TutorialScene from "../../scene/TutorialScene/TutorialScene.jsx";

// ---------------------- imports characters ---------------------- //
import decepticons from "../../datas/characters/decepticons/decepticons";
import autobots from "../../datas/characters/autobots/autobots";

const Level_1_0 = ({ audioPlay,setPlayerEndLevel,setShowGlobalElement }) => {
  return (
    <>
      <TutorialScene
        autobots={autobots.tutorialBot}
        decepticons={decepticons.decepticonModel}
        audioPlay={audioPlay}
        setPlayerEndLevel={setPlayerEndLevel} setShowGlobalElement={setShowGlobalElement}
      />
    </>
  );
};

export default Level_1_0;
