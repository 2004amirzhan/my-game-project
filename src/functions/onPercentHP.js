const onPercentHP = (allHP, damage, percentLine) => {
  let percent = Math.round((100 * damage) / allHP + percentLine);
  return percent;
};

export default onPercentHP;
