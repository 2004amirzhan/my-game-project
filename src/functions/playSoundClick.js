const playSoundClick = (soundSrc) => {
  const audio = new Audio();
  const sounds = soundSrc;
  audio.src = sounds;

  audio.autoplay = true;
};

export default playSoundClick;
