const preloader = () => {
  const preloader = document.getElementById("Preloader");
  if (!preloader.classList.contains("hide")) {
    preloader.classList.add("hide");
  }
};

export default preloader;
