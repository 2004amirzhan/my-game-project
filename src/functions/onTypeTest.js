const onTypeTest = (type, fraction) => {
  if (type === "Легкий") {
    return (
      <img
        src={`/images/components/fractions/${fraction}s/${fraction}-easy.png`}
        className="typeIcon"
        alt="fraction"
      />
    );
  } else if (type === "Средний") {
    return (
      <img
        src={`/images/components/fractions/${fraction}s/${fraction}-middle.png`}
        className="typeIcon"
        alt="fraction"
      />
    );
  } else if (type === "Тяжелый") {
    return (
      <img
        src={`/images/components/fractions/${fraction}s/${fraction}-heavy.png`}
        className="typeIcon"
        alt="fraction"
      />
    );
  } else if (type === "Летающий") {
    return (
      <img
        src={`/images/components/fractions/${fraction}s/${fraction}-fly.png`}
        className="typeIcon"
        alt="fraction"
      />
    );
  }
};

export default onTypeTest;
